#include<bits/stdc++.h>
#define MAXN 1000
using namespace std;
int n;
const int m=3;

struct task{
    int id;
    int st;
    int l1;
    int l2;
    int l3;
    bool f1;
    bool f2;
    bool f3;
    int s1;
    int s2;
    int s3;
};

int answer = INT_MAX;
int start_answer[3][MAXN];

int answer_new;
int start_new[3][MAXN];


struct task tasks[MAXN];

void sort_tasks(){
    for(int i=0 ; i<n ; i++){
        for(int j=0 ; j<i ; j++){
            if(tasks[i].st < tasks[j].st){
                struct task task_tmp = tasks[i];
                tasks[i] = tasks[j];
                tasks[j] = task_tmp;
            }
        }
    }
}

void print_tasks(){
    for(int i=0 ; i<n ; i++){
        cout<<i<<": "<<tasks[i].st<<" "<<tasks[i].l1<<" "<<tasks[i].l2<<" "<<tasks[i].l3<<"\n";
    }
}

vector<int> vec1;
vector<int> vec2;
vector<int> vec3;
int t;
bool vec1_wait;
bool vec2_wait;
bool vec3_wait;


bool is_workstation_free(int num){
    if(num==1){
        if(vec1.size()==0 || tasks[vec1[vec1.size()-1]].f1==true){
            if(vec1_wait==false){
                return true;
            }
        }
    }
    else if(num==2){
        if(vec2.size()==0 || tasks[vec2[vec2.size()-1]].f2==true){
            if(vec2_wait==false){
                return true;
            }
        }
    }
    else{
        if(vec3.size()==0 || tasks[vec3[vec3.size()-1]].f3==true){
            if(vec3_wait==false){
                return true;
            }

        }
    }
    return false;
}

int get_workstation_free_turn(){
    static int turn=0;
    turn++;
    for(int i=0 ; i<3 ; i++){
        if(is_workstation_free((turn+i)%3)){
            if((turn+i)%3){
                return (turn+i)%3;
            }
            else{
                return 3;
            }
        }
    }
    return 0;
}

int get_workstation_free(){
    return get_workstation_free_turn();
}



bool task_overlap(int num_task){
    if(vec1.size()==0 || vec1[vec1.size()-1]!=num_task || tasks[num_task].f1==true){
        if(vec2.size()==0 || vec2[vec2.size()-1]!=num_task || tasks[num_task].f2==true){
            if(vec3.size()==0 || vec3[vec3.size()-1]!=num_task || tasks[num_task].f3==true){
                return false;
            }
        }
    }
    return true;
}

void print_vectors(){
    cout<<"1:\n";
    for(int i=0 ; i<vec1.size() ; i++){
        cout<<vec1[i]<<" ";
    }
    cout<<"\n2:\n";
    for(int i=0 ; i<vec2.size() ; i++){
        cout<<vec2[i]<<" ";
    }
    cout<<"\n3:\n";
    for(int i=0 ; i<vec3.size() ; i++){
        cout<<vec3[i]<<" ";
    }

}

bool is_finish(struct task t){
    return t.f1 && t.f2 && t.f3;
}

bool is_finish(){
    for(int i=0 ; i<n ; i++){
        if(is_finish(tasks[i])==false){
            return false;
        }
    }
    return true;
}

void set_finish_task(){
    if(vec1.size()>0 && tasks[vec1[vec1.size()-1]].s1+tasks[vec1[vec1.size()-1]].l1==t && tasks[vec1[vec1.size()-1]].f1==false){
        tasks[vec1[vec1.size()-1]].f1=true;
        cout<<"1 -> "<<vec1[vec1.size()-1]<<" | "<<tasks[vec1[vec1.size()-1]].s1<<" : "<<t<<endl;
        answer_new = t;
        start_new[1][tasks[vec1[vec1.size()-1]].id] = tasks[vec1[vec1.size()-1]].s1;
    }
    if(vec2.size()>0 && tasks[vec2[vec2.size()-1]].s2+tasks[vec2[vec2.size()-1]].l2==t && tasks[vec2[vec2.size()-1]].f2==false){
        tasks[vec2[vec2.size()-1]].f2=true;
        cout<<"2 -> "<<vec2[vec2.size()-1]<<" | "<<tasks[vec2[vec2.size()-1]].s2<<" : "<<t<<endl;
        answer_new = t;
        start_new[2][tasks[vec2[vec2.size()-1]].id] = tasks[vec2[vec2.size()-1]].s2;

    }
    if(vec3.size()>0 && tasks[vec3[vec3.size()-1]].s3+tasks[vec3[vec3.size()-1]].l3==t && tasks[vec3[vec3.size()-1]].f3==false){
        tasks[vec3[vec3.size()-1]].f3=true;
        cout<<"3 -> "<<vec3[vec3.size()-1]<<" | "<<tasks[vec3[vec3.size()-1]].s3<<" : "<<t<<endl;
        answer_new = t;
        start_new[0][tasks[vec3[vec3.size()-1]].id] = tasks[vec3[vec3.size()-1]].s3;
    }
}

void bigest_first(){
    int task_active=0; /// 0 --> task_active-1
    t =0 ;
    vec1_wait = false;
    vec1_wait = false;
    vec1_wait = false;

    while(true){
        if(is_finish()){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }
        int work_station = get_workstation_free();
        struct task new_task;
        if(work_station==0){
            t++;
            set_finish_task();
            vec1_wait = false;
            vec2_wait = false;
            vec3_wait = false;
        }
        else if(work_station==1){
            int maxim = 0;
            int maxim_task=-1;


            for(int i=0 ; i<task_active ; i++){
                new_task = tasks[i];
                if(new_task.f1==false){
                    if(task_overlap(i)==false){
                        if(maxim<new_task.l1){
                            maxim = new_task.l1;
                            maxim_task = i;
                        }
                    }
                }
            }
            if(maxim==0){
                vec1_wait = true;
            }
            else{
                vec1.push_back(maxim_task);
                tasks[maxim_task].s1 = t;
            }
        }
        else if(work_station==2){
            int maxim = 0;
            int maxim_task=-1;
            for(int i=0 ; i<task_active ; i++){
                new_task = tasks[i];
                if(new_task.f2==false){
                    if(task_overlap(i)==false){
                        if(maxim<new_task.l2){
                            maxim = new_task.l2;
                            maxim_task = i;
                        }
                    }
                }
            }
            if(maxim==0){
                vec2_wait = true;
            }
            else{
                vec2.push_back(maxim_task);
                tasks[maxim_task].s2 = t;
            }
        }
        else if(work_station==3){
            int maxim = 0;
            int maxim_task=-1;
          /*  if(vec3.size()>0){
                tasks[vec3[vec3.size()-1]].f3=true;
                cout<<"f3 finish : "<<vec3[vec3.size()-1]<<endl;

            }*/
            for(int i=0 ; i<task_active ; i++){
                new_task = tasks[i];
                if(new_task.f3==false){
                    if(task_overlap(i)==false){
                        if(maxim<new_task.l3){
                            maxim = new_task.l3;
                            maxim_task = i;
                        }
                    }
                }
            }
            if(maxim==0){
                vec3_wait = true;
            }
            else{
                vec3.push_back(maxim_task);
                tasks[maxim_task].s3 = t;
            }
        }

    }

}

void prepare(){
    for(int i=0 ; i<n ; i++){
        tasks[i].f1 = false;
        tasks[i].f2 = false;
        tasks[i].f3 = false;
        tasks[i].s1 = INT_MAX;
        tasks[i].s2 = INT_MAX;
        tasks[i].s3 = INT_MAX;
    }
    vec1.clear();
    vec2.clear();
    vec3.clear();
}

void output(){
    cout<<answer<<endl;
    for(int i=0 ; i<n ; i++){
        cout<<start_answer[1][i]<<" "<<start_answer[2][i]<<" "<<start_answer[0][i]<<"\n";
    }
}

void run(){
    prepare();
    bigest_first();
    cout<<endl;
    if(answer_new<answer){
        answer = answer_new;
        for(int i=0 ; i<n ; i++){
            for(int j=0 ; j<m ; j++){
                start_answer[j][i] = start_new[j][i];
            }
        }
    }
    prepare();
    bigest_first();
}


int main(){
    int inp_m;
    cin>>n>>inp_m;
    if(inp_m!=3){
        cout<<"ERROR: m must be 3\n";
        return 0;
    }
    if(n<0 || n>1000){
        cout<<"ERROR: 0<=n and n<=1000\n";
        return 0;
    }
    for(int i=0 ; i<n ; i++){
        cin>>tasks[i].st>>tasks[i].l1>>tasks[i].l2>>tasks[i].l3;
        tasks[i].id = i;
    }
    sort_tasks();
    print_tasks();
    run();
    output();

    return 0;
}
