#include<bits/stdc++.h>
#define MAXN 1000
#define NOTHING -1
using namespace std;
int n;
const int m=3;

struct task{
    int id;
    int st;
    int l[3];
    bool f[3];
    int s[3];
};

int answer = INT_MAX;
int start_answer[3][MAXN];

int answer_new;
int start_new[3][MAXN];

int answer_min_possible;

struct task tasks[MAXN];
int task_active;
int turn;
int t;
vector<int> vec[3];
bool vec_wait[3];
int random_test;

void sort_tasks(){
    for(int i=0 ; i<n ; i++){
        for(int j=0 ; j<i ; j++){
            if(tasks[i].st < tasks[j].st){
                struct task task_tmp = tasks[i];
                tasks[i] = tasks[j];
                tasks[j] = task_tmp;
            }
        }
    }
}

void print_tasks(){
    for(int i=0 ; i<n ; i++){
        cout<<i<<": "<<tasks[i].st<<" "<<tasks[i].l[0]<<" "<<tasks[i].l[1]<<" "<<tasks[i].l[2]<<"\n";
    }
}

int top(int num){
    return vec[num][vec[num].size()-1];
};

bool is_workstation_free(int num){
    if(vec[num].size()==0 || tasks[top(num)].f[num]==true){
        if(vec_wait[num]==false){
            return true;
        }
    }
    return false;
}

int get_workstation_free_randomize(int order){
    turn = rand()%3;
    for(int i = 0 ; i<3 ; i++){
        if(is_workstation_free(((((turn+i*order)%3)+3)%3))){
            return (((((turn+i*order)%3)+3)%3));
        }
    }
    return NOTHING;
}

int get_workstation_free_circular(int order){
    turn++;
    for(int i=0 ; i<3 ; i++){
        if(is_workstation_free((((turn+i*order)%3)+3)%3)){
            return ((((turn+i*order)%3)+3)%3);
        }
    }
    return NOTHING;
}

int get_workstation_free_fixed(int order){
    for(int i=0 ; i<3 ; i++){
        if(is_workstation_free((((turn+i*order)%3)+3)%3)){
            return ((((turn+i*order)%3)+3)%3);
        }
    }
    return NOTHING;
}

int get_workstation_free(string type_turn,int order){
    if(type_turn=="Circular"){
        return get_workstation_free_circular(order);
    }
    else if(type_turn=="Fixed"){
        return get_workstation_free_fixed(order);
    }
    else if(type_turn=="Randomize"){
        return get_workstation_free_randomize(order);
    }
}

int get_workstation_free_dual_circular(int work_station_priority){
    turn++;
    turn%=3;
    if(turn==work_station_priority){
        turn++;
        turn%=3;
    }
    if(is_workstation_free(turn)){
        return turn;
    }
    else{
        for(int i=0 ; i<3 ; i++){
            if(i!=turn && i!=work_station_priority){
                if(is_workstation_free(i)){
                    return i;
                }
            }
        }
    }
    return NOTHING;
}

int get_workstation_free_dual_fixed(int work_station_priority){
    if(is_workstation_free(turn)){
        return turn;
    }
    else{
        for(int i=0 ; i<3 ; i++){
            if(i!=turn && i!=work_station_priority){
                if(is_workstation_free(i)){
                    return i;
                }
            }
        }
    }
    return NOTHING;
}

int get_workstation_free_dual_randomize(int work_station_priority){
    turn = rand()%3;
    if(turn==work_station_priority){
        turn++;
        turn%=3;
    }
    if(is_workstation_free(turn)){
        return turn;
    }
    else{
        for(int i=0 ; i<3 ; i++){
            if(i!=turn && i!=work_station_priority){
                if(is_workstation_free(i)){
                    return i;
                }
            }
        }
    }
    return NOTHING;
}

int get_workstation_free_dual(string type_turn,int work_station_priority){
    if(type_turn=="Circular"){
        return get_workstation_free_dual_circular(work_station_priority);
    }
    else if(type_turn=="Fixed"){
        return get_workstation_free_dual_fixed(work_station_priority);
    }
    else if(type_turn=="Randomize"){
        return get_workstation_free_dual_randomize(work_station_priority);
    }
}

bool task_overlap(int num_task){
    for(int i=0 ; i<3 ; i++){
        if(vec[i].size() >0 && top(i)==num_task && tasks[num_task].f[i]==false){
            return true;
        }
    }
    return false;

}

bool task_overlap(int num_task,int work_station_priority,int work_station){
    for(int i=0 ; i<3 ; i++){
        if(i!=work_station && i!=work_station_priority){
            if(vec[i].size() >0 && top(i)==num_task && tasks[num_task].f[i]==false){
                return true;
            }
        }
    }
    if(vec[work_station_priority].size()>0){
        if(t + tasks[num_task].l[work_station] > tasks[num_task].s[work_station_priority] &&
            t < tasks[num_task].s[work_station_priority] +tasks[num_task].l[work_station_priority]
            ){
                return true;
        }
    }

    return false;

}

bool is_finish_workstation(int num){
    for(int i=0 ; i<n ; i++){
        if(tasks[i].f[num]==false){
            return false;
        }
    }
    return true;
}

bool is_finish_except(int num){
    for(int i=0 ; i<3 ; i++){
        if(i!=num && is_finish_workstation(i)==false){
            return false;
        }
    }
    return true;
}

bool is_finish(){
    for(int i=0 ; i<3 ; i++){
        if(is_finish_workstation(i)==false){
            return false;
        }
    }
    return true;
}

void set_finish_task(){
    for(int i=0 ; i<3 ; i++){
        if(vec[i].size()>0 && tasks[top(i)].s[i]+tasks[top(i)].l[i]==t && tasks[top(i)].f[i]==false){
            tasks[top(i)].f[i]=true;
            answer_new = max(t,answer_new);
            start_new[i][tasks[top(i)].id] = tasks[top(i)].s[i];
        }
    }
}

void extreme_serial_dual(string type_turn,char operation,int work_station_priority){
    while(true){
        if(is_finish_except(work_station_priority)){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }
        int work_station = get_workstation_free_dual(type_turn,work_station_priority);
        struct task new_task;
        if(work_station==NOTHING){
            t++;
            set_finish_task();
            vec_wait[0] = false;
            vec_wait[1] = false;
            vec_wait[2] = false;
        }
        else{
            int extreme;
            if(operation=='<'){
                extreme = INT_MAX;

            }
            else{
                extreme = 0;
            }

            int extreme_task=-1;
            for(int i=0 ; i<task_active ; i++){
                new_task = tasks[i];
                if(new_task.f[work_station]==false){
                    if(task_overlap(i,work_station_priority,work_station)==false){
                        if(extreme>new_task.l[work_station] && operation=='<'){
                            extreme = new_task.l[work_station];
                            extreme_task = i;
                        }
                        else if(extreme<new_task.l[work_station] && operation=='>'){
                            extreme = new_task.l[work_station];
                            extreme_task = i;
                        }
                    }
                }
            }
            if(extreme==INT_MAX || extreme==0){
                vec_wait[work_station] = true;
            }
            else{
                vec[work_station].push_back(extreme_task);
                tasks[extreme_task].s[work_station] = t;
            }
        }

    }
}

void extreme_serial(string type_turn,int order,char operation){
    while(true){
        if(is_finish()){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }
        int work_station = get_workstation_free(type_turn,order);
        struct task new_task;
        if(work_station==NOTHING){
            t++;
            set_finish_task();
            vec_wait[0] = false;
            vec_wait[1] = false;
            vec_wait[2] = false;
        }
        else{
            int extreme;
            if(operation=='<'){
                extreme = INT_MAX;

            }
            else{
                extreme = 0;
            }

            int extreme_task=-1;


            for(int i=0 ; i<task_active ; i++){
                new_task = tasks[i];
                if(new_task.f[work_station]==false){
                    if(task_overlap(i)==false){
                        if(extreme>new_task.l[work_station] && operation=='<'){
                            extreme = new_task.l[work_station];
                            extreme_task = i;
                        }
                        else if(extreme<new_task.l[work_station] && operation=='>'){
                            extreme = new_task.l[work_station];
                            extreme_task = i;
                        }
                    }
                }
            }
            if(extreme==INT_MAX || extreme==0){
                vec_wait[work_station] = true;
            }
            else{
                vec[work_station].push_back(extreme_task);
                tasks[extreme_task].s[work_station] = t;
            }
        }

    }
}

void extreme_max_priority(int max_priority,char operation){
    while(true){
        if(is_finish_workstation(max_priority)){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }

        int extreme;
        if(operation=='>'){
            extreme = 0;
        }
        else{
            extreme = INT_MAX;
        }

        int extreme_task=-1;
        struct task new_task;
        for(int i=0 ; i<task_active ; i++){
            new_task = tasks[i];
            if(new_task.f[max_priority]==false){
                if(extreme<new_task.l[max_priority] && operation=='>'){
                        extreme = new_task.l[max_priority];
                        extreme_task = i;
                }
                else if(extreme>new_task.l[max_priority] && operation=='<'){
                        extreme = new_task.l[max_priority];
                        extreme_task = i;
                }
            }
        }
        if(extreme==0 || extreme==INT_MAX){
            t++;
        }
        else{
            vec[max_priority].push_back(extreme_task);
            tasks[extreme_task].s[max_priority] = t;
            t = t + tasks[extreme_task].l[max_priority];

            tasks[extreme_task].f[max_priority]=true;
           // cout<<i<<" -> "<<top(i)<<" | "<<tasks[top(i)].s[i]<<" : "<<t<<endl;
            answer_new = t;
            start_new[max_priority][tasks[extreme_task].id] = tasks[extreme_task].s[max_priority];

        }

    }

}

void extreme_middle_priority(int middle_priority,int max_priority,char operation){
    while(true){
        if(is_finish_workstation(middle_priority)){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }

        int extreme;
        if (operation == '>'){
            extreme = 0;
        }
        else{
            extreme = INT_MAX;
        }
        int extreme_task=-1;
        struct task new_task;
        for(int i=0 ; i<task_active ; i++){
            new_task = tasks[i];
            if(new_task.f[middle_priority]==false){
                if(t>=new_task.l[max_priority]+new_task.s[max_priority] || t+new_task.l[middle_priority]<=new_task.s[max_priority]){
                    if(extreme<new_task.l[middle_priority] && operation=='>'){
                            extreme = new_task.l[middle_priority];
                            extreme_task = i;
                    }
                    else if(extreme>new_task.l[middle_priority] && operation=='<'){
                            extreme = new_task.l[middle_priority];
                            extreme_task = i;
                    }
                }
            }
        }
        if(extreme==0 || extreme == INT_MAX){
            t++;
        }
        else{
            vec[middle_priority].push_back(extreme_task);
            tasks[extreme_task].s[middle_priority] = t;
            t = t + tasks[extreme_task].l[middle_priority];
            tasks[extreme_task].f[middle_priority]=true;
           // cout<<i<<" -> "<<top(i)<<" | "<<tasks[top(i)].s[i]<<" : "<<t<<endl;
            answer_new = max(t,answer_new);
            start_new[middle_priority][tasks[extreme_task].id] = tasks[extreme_task].s[middle_priority];

        }

    }

}

void extreme_min_priority(int min_priority,int middle_priority,int max_priority , char operation){
    while(true){
        if(is_finish_workstation(min_priority)){
            break;
        }
        while(task_active<n && t>=tasks[task_active].st){
            task_active++;
        }

        int extreme;
        if (operation == '>'){
            extreme = 0;
        }
        else{
            extreme = INT_MAX;
        }
        int extreme_task=-1;
        struct task new_task;
        for(int i=0 ; i<task_active ; i++){
            new_task = tasks[i];
            if(new_task.f[min_priority]==false){
                if(t>=new_task.l[max_priority]+new_task.s[max_priority] || t+new_task.l[min_priority]<=new_task.s[max_priority]){
                    if(t>=new_task.l[middle_priority]+new_task.s[middle_priority] || t+new_task.l[min_priority]<=new_task.s[middle_priority]){
                        if(extreme<new_task.l[min_priority] && operation=='>'){
                            extreme = new_task.l[min_priority];
                            extreme_task = i;
                        }
                        else if(extreme>new_task.l[min_priority] && operation=='<'){
                                extreme = new_task.l[min_priority];
                                extreme_task = i;
                        }
                    }
                }
            }
        }
        if(extreme==0 || extreme == INT_MAX){
            t++;
        }
        else{
            vec[min_priority].push_back(extreme_task);
            tasks[extreme_task].s[min_priority] = t;
            t = t + tasks[extreme_task].l[min_priority];
            tasks[extreme_task].f[min_priority]=true;
           // cout<<i<<" -> "<<top(i)<<" | "<<tasks[top(i)].s[i]<<" : "<<t<<endl;
            answer_new = max(t,answer_new);
            start_new[min_priority][tasks[extreme_task].id] = tasks[extreme_task].s[min_priority];

        }
    }
}

void prepare(){
    for(int i=0 ; i<n ; i++){
        for(int j=0 ; j<3 ; j++){
            tasks[i].f[j] = false;
            tasks[i].s[j] = INT_MAX;
        }
    }
    vec[0].clear();
    vec[1].clear();
    vec[2].clear();

    task_active=0;
    t =0 ;
    turn =0 ;
    answer_new = 0;
    vec_wait[0] = false;
    vec_wait[1] = false;
    vec_wait[2] = false;
}

void output(){
    cout<<answer<<endl;
    for(int i=0 ; i<n ; i++){
        cout<<start_answer[0][i]<<" "<<start_answer[1][i]<<" "<<start_answer[2][i]<<"\n";
    }
}

void check_answer(){
   // cout<<answer_new<<endl<<endl;
    if(answer_new<answer){
        answer = answer_new;
        cout<<answer_new<<endl<<endl;
        for(int i=0 ; i<n ; i++){
            for(int j=0 ; j<m ; j++){
                start_answer[j][i] = start_new[j][i];
            }
        }
    }
    if(answer_min_possible==answer){
        output();
        exit(0);
    }

}

void extreme_serial_randomize(int order,char operation){
    prepare();
    extreme_serial("Randomize",order,operation);
    check_answer();
}

void extreme_serial_circular(int start_turn,int order,char operation){
    prepare();
    turn = start_turn;
    extreme_serial("Circular",order,operation);
    check_answer();
}

void extreme_serial_fixed(int start_turn,int order,char operation){
    prepare();
    turn = start_turn;
    extreme_serial("Fixed",order,operation);
    check_answer();
}

int total_work(int num){
    int out = 0;
    for(int i=0 ; i<n ; i++){
        out += tasks[i].l[num];
    }
    return out;
}

void calculate_answer_min_possible(){
    prepare();
    while(true){
        if(is_finish()){
            break;
        }
        for(int i=0 ; i<3 ; i++){
            if(vec[i].size()==0){
                if(tasks[0].st==t){
                    vec[i].push_back(0);
                    tasks[0].s[i]=t;
                }
            }
            else{
                if(tasks[top(i)].s[i]+tasks[top(i)].l[i]<=t){
                    tasks[top(i)].f[i] = true;
                    if(vec[i].size()<n && tasks[top(i)+1].st<=t){
                        vec[i].push_back(top(i)+1);
                        tasks[top(i)].s[i] = t;
                    }
                }
            }

        }
        t++;
    }
    t--;
    answer_min_possible = t;
    cout<<"answer minim possible:"<<answer_min_possible<<endl;
}

int* get_order_workstation(){
    int tot[3];
    int max_priority;
    int middle_priority;
    int min_priority;

    for(int i=0 ; i<3 ; i++){
        tot[i] = total_work(i);
    }

    if(tot[0]<tot[1]){
        if(tot[1]<tot[2]){
            min_priority = 0;
            middle_priority = 1;
            max_priority = 2;
        }
        else{
            max_priority = 1;
            if(tot[0] < tot[2]){
                min_priority = 0;
                middle_priority = 2;
            }
            else{
                min_priority = 2;
                middle_priority = 0;
            }
        }
    }
    else{
        if(tot[0]<tot[2]){
            min_priority = 1;
            middle_priority = 0;
            max_priority = 2;
        }
        else{
            max_priority = 0;
            if(tot[1] < tot[2]){
                min_priority = 1;
                middle_priority = 2;
            }
            else{
                min_priority = 2;
                middle_priority = 1;
            }
        }
    }
    int out[3];
    out[0] = min_priority;
    out[1] = middle_priority;
    out[2] = max_priority;
    return out;
}

void priority(int max_priority,int middle_priority,int min_priority,char max_operation,char middle_operation,char min_operation){
    prepare();
    extreme_max_priority(max_priority,max_operation);
    t=0;
    task_active =0;
    extreme_middle_priority(middle_priority,max_priority,middle_operation);
    t=0;
    task_active =0;
    extreme_min_priority(min_priority,middle_priority,max_priority,min_operation);
    check_answer();
}

void mixed_first_priority(string type_turn,char max_operation,char turn_operation,int max_priority,int turn_first){
    prepare();
    extreme_max_priority(max_priority,max_operation);
    t=0;
    task_active=0;
    turn = turn_first;
    extreme_serial_dual(type_turn,turn_operation,max_priority);
    check_answer();
}

void mixed_first_priority_randomize(char max_operation,char turn_operation,int max_priority){
    prepare();
    extreme_max_priority(max_priority,max_operation);
    t=0;
    task_active=0;
    extreme_serial_dual("Randomize",turn_operation,max_priority);
    check_answer();
}

void mixed_first_serial(string type_turn,char min_operation,char turn_operation,int min_priority,int middle_priority,int max_priority,int turn_first){
    prepare();
    turn = turn_first;
    extreme_serial_dual(type_turn,turn_operation,min_priority);
    t=0;
    task_active=0;
    extreme_min_priority(min_priority,middle_priority,max_priority,min_operation);
    check_answer();
}

void mixed_first_serial_randomize(char min_operation,char turn_operation,int min_priority,int middle_priority,int max_priority){
    prepare();
    extreme_serial_dual("Randomize",turn_operation,min_priority);
    t=0;
    task_active=0;
    extreme_min_priority(min_priority,middle_priority,max_priority,min_operation);
    check_answer();

}

void mixed_first_serial_algorithm(){
    prepare();
    int* order_total  = get_order_workstation();
    int min_priority = order_total[0];
    int middle_priority = order_total[1];
    int max_priority = order_total[2];

    mixed_first_serial("Fixed",'>','>',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Fixed",'>','>',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Fixed",'>','<',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Fixed",'>','<',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Fixed",'<','>',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Fixed",'<','>',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Fixed",'<','<',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Fixed",'<','<',min_priority,middle_priority,max_priority,max_priority);

    mixed_first_serial("Circular",'>','>',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Circular",'>','>',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Circular",'>','<',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Circular",'>','<',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Circular",'<','>',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Circular",'<','>',min_priority,middle_priority,max_priority,max_priority);
    mixed_first_serial("Circular",'<','<',min_priority,middle_priority,max_priority,middle_priority);
    mixed_first_serial("Circular",'<','<',min_priority,middle_priority,max_priority,max_priority);

}

void mixed_first_serial_randomize_algorithm(){
    prepare();
    int* order_total  = get_order_workstation();
    int min_priority = order_total[0];
    int middle_priority = order_total[1];
    int max_priority = order_total[2];

    for(int i=0 ; i<random_test ; i++){
        mixed_first_serial_randomize('>','>',min_priority,middle_priority,max_priority);
        mixed_first_serial_randomize('>','<',min_priority,middle_priority,max_priority);
        mixed_first_serial_randomize('<','>',min_priority,middle_priority,max_priority);
        mixed_first_serial_randomize('<','<',min_priority,middle_priority,max_priority);
    }
}

void mixed_first_priority_algorithm(){
    prepare();
    int* order_total  = get_order_workstation();
    int min_priority = order_total[0];
    int middle_priority = order_total[1];
    int max_priority = order_total[2];

    mixed_first_priority("Circular",'>','>',max_priority,min_priority);
    mixed_first_priority("Circular",'>','>',max_priority,middle_priority);
    mixed_first_priority("Circular",'>','<',max_priority,min_priority);
    mixed_first_priority("Circular",'>','<',max_priority,middle_priority);
    mixed_first_priority("Circular",'<','>',max_priority,min_priority);
    mixed_first_priority("Circular",'<','>',max_priority,middle_priority);
    mixed_first_priority("Circular",'<','<',max_priority,min_priority);
    mixed_first_priority("Circular",'<','<',max_priority,middle_priority);

    mixed_first_priority("Fixed",'>','>',max_priority,min_priority);
    mixed_first_priority("Fixed",'>','>',max_priority,middle_priority);
    mixed_first_priority("Fixed",'>','<',max_priority,min_priority);
    mixed_first_priority("Fixed",'>','<',max_priority,middle_priority);
    mixed_first_priority("Fixed",'<','>',max_priority,min_priority);
    mixed_first_priority("Fixed",'<','>',max_priority,middle_priority);
    mixed_first_priority("Fixed",'<','<',max_priority,min_priority);
    mixed_first_priority("Fixed",'<','<',max_priority,middle_priority);
}

void mixed_first_priority_randomize_algorithm(){
    prepare();
    int* order_total  = get_order_workstation();
    int min_priority = order_total[0];
    int middle_priority = order_total[1];
    int max_priority = order_total[2];

    for(int i=0 ; i<random_test ; i++){
        mixed_first_priority_randomize('>','>',max_priority);
        mixed_first_priority_randomize('>','<',max_priority);
        mixed_first_priority_randomize('<','>',max_priority);
        mixed_first_priority_randomize('<','<',max_priority);

    }

}
void priority_algorithm(){
    prepare();
    int* order_total  = get_order_workstation();
    int min_priority = order_total[0];
    int middle_priority = order_total[1];
    int max_priority = order_total[2];

    priority(max_priority,middle_priority,min_priority,'>','>','>');
    priority(max_priority,middle_priority,min_priority,'>','>','<');
    priority(max_priority,middle_priority,min_priority,'>','<','>');
    priority(max_priority,middle_priority,min_priority,'>','<','<');
    priority(max_priority,middle_priority,min_priority,'<','>','>');
    priority(max_priority,middle_priority,min_priority,'<','>','<');
    priority(max_priority,middle_priority,min_priority,'<','<','>');
    priority(max_priority,middle_priority,min_priority,'<','<','<');
}

void serial_algorithm(){
    for(int i=0 ; i<3 ; i++){
        extreme_serial_circular(i,+1,'<');
        extreme_serial_circular(i,+1,'>');
        extreme_serial_circular(i,-1,'<');
        extreme_serial_circular(i,-1,'>');

        extreme_serial_fixed(i,+1,'<');
        extreme_serial_fixed(i,+1,'>');
        extreme_serial_fixed(i,-1,'<');
        extreme_serial_fixed(i,-1,'>');

    }

}

void serial_randomize_algorithm(){
    for(int i=0 ; i<random_test ; i++){

        extreme_serial_randomize(+1,'<');
        extreme_serial_randomize(+1,'>');
        extreme_serial_randomize(-1,'<');
        extreme_serial_randomize(-1,'>');
    }
}

void run(){
    priority_algorithm();

    serial_algorithm();

    mixed_first_priority_algorithm();

    mixed_first_serial_algorithm();

   // serial_randomize_algorithm();

    //mixed_first_serial_randomize_algorithm();

    mixed_first_priority_randomize_algorithm();
}

void preprocess(){
    srand(time(NULL));
    sort_tasks();
    calculate_answer_min_possible();
    random_test = 500*1000*1000/n/answer_min_possible;
}

void input(){
    int inp_m;
    cin>>n>>inp_m;
    if(inp_m!=3){
        cout<<"ERROR: m must be 3\n";
        exit(-1);
    }
    if(n<0 || n>1000){
        cout<<"ERROR: 0<=n and n<=1000\n";
        exit(-1);
    }
    for(int i=0 ; i<n ; i++){
        cin>>tasks[i].st>>tasks[i].l[0]>>tasks[i].l[1]>>tasks[i].l[2];
        tasks[i].id = i;
    }
}

int main(){
    input();
    preprocess();
    run();
    output();

    return 0;
}
