#include<bits/stdc++.h>
#define MAXN 1100

using namespace std;

int n;
const int m=3;


struct task{
    int id;
    int st;
    int l[3];
    bool f[3];
    int s[3];
};

struct task tasks[MAXN];

int answer;
int start_answer[3][MAXN];

void read_input(){
    int inp_m;

    std::fstream myfile("input_group5.txt", std::ios_base::in);


    myfile>>n>>inp_m;
    if(inp_m!=3){
        cout<<"ERROR: m must be 3\n";
        exit(-1);
    }
    if(n<0 || n>1000){
        cout<<"ERROR: 0<=n and n<=1000\n";
        exit(-1);
    }
    for(int i=0 ; i<n ; i++){
        myfile>>tasks[i].st>>tasks[i].l[0]>>tasks[i].l[1]>>tasks[i].l[2];
        tasks[i].id = i;
    }
}

void read_output(){
    std::fstream myfile("output_from_1_to_5.txt", std::ios_base::in);
/*    while(true){
        int tmp ;
        myfile>>tmp;
        cout<<tmp<<endl;
    }*/
    myfile>>answer;

    for(int i=0 ; i<n ;i++){
        myfile>>tasks[i].s[0]>>tasks[i].s[1]>>tasks[i].s[2];
    }
}

void print_tasks(){
    for(int i=0 ; i<n ; i++){
        cout<<i<<" : "<<"\n";
        cout<<"Answer"<<answer<<endl;
        cout<<"Start:"<<tasks[i].st<<"\n";
        cout<<tasks[i].l[0]<<" "<<tasks[i].l[1]<<" "<<tasks[i].l[2]<<"\n";
        cout<<tasks[i].s[0]<<" "<<tasks[i].s[1]<<" "<<tasks[i].s[2]<<"\n\n";

    }
}

bool check_start_time(){
    bool flag = true;
    for(int i=0 ; i<n ; i++){
        for(int j=0 ; j<3 ; j++){
            if(tasks[i].s[j]<tasks[i].st){
                cerr<<"ERROR START_TIME_ERROR : "<<"task #"<<i+1<<" in workstation #"<<j+1<<endl;
                flag = false;
            }
        }
    }
    return flag;
}

bool check_finish_time(){
    int maxim = 0;
    int num_task;
    int num_work_station;
    for(int i=0 ; i<n ; i++){
        for(int j=0 ; j<3 ; j++){
            if(maxim<tasks[i].s[j]+tasks[i].l[j]){
                maxim = tasks[i].s[j]+tasks[i].l[j];
                num_task = i;
                num_work_station = j;
            }
        }
    }
    if(maxim>answer){
        cerr<<"ERROR FINISH_TIME_ERROR : "<<"task #"<<num_task+1<<" in workstation #"<<num_work_station+1<<" finished in "<<maxim<<endl;
        return false;
    }
    else{
        return true;
    }
}

bool task_overlap(int WS1,int WS2,int i){
    if(tasks[i].s[WS2]< tasks[i].s[WS1]+tasks[i].l[WS1] &&
         tasks[i].s[WS2]+tasks[i].l[WS2] > tasks[i].s[WS1]){
        cerr<<"ERROR TASK_OVERLAP_ERROR : "<<"task #"<<i+1<<" in workstations "<<WS1+1<<","<<WS2+1<<" has overlap"<<endl;
        return true;
    }
    return false;
}

bool check_task_overlap(){
    bool flag = true;
    for(int i=0 ; i<n ; i++){
        if(task_overlap(0,1,i) | task_overlap(0,2,i) | task_overlap(1,2,i)){
            flag = false;
        }
    }
    return flag;
}

bool check_workstation_overlap(){
    bool flag = true;
    for(int j=0 ; j<3 ; j++){
        set<pair<int,int> > myset;
        myset.clear();

        for(int i=0 ; i<n ; i++){
            myset.insert(make_pair(tasks[i].s[j],i));
        }

        pair<int,int> sorted[MAXN];
        int counter=0;
        for(set<pair<int,int> > :: iterator it = myset.begin() ; it != myset.end() ; counter++,it++){
            sorted[counter] = *it;
        }

        for(int i=0 ; i<n-1 ; i++){
            int num = sorted[i].second;
            int num_next = sorted[i+1].second;
            if(tasks[num].s[j] +  tasks[num].l[j] > tasks[num_next].s[j]){
                flag = false;
                cerr<<"ERROR WORKSTATION_OVERLAP : "<<"workstation #"<<j+1<<" between task "<<"#"<<num+1<<", #"<<num_next+1<<" has overlap"<<endl;
            }
        }

    }
    return flag;
}

int main(){
    read_output();
    read_input();
    print_tasks();

    if(check_start_time() & check_finish_time() & check_task_overlap() & check_workstation_overlap()){
        cout<<"Correct !";
    }
    else{
        cout<<"Incorrect !";
    }




    return 0;
}
